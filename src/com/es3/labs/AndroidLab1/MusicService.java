package com.es3.labs.AndroidLab1;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class MusicService extends Service {

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}
	
	@Override
	public void onCreate() {
		Log.d("MusicService", "Created MusicService");
		Toast.makeText(getApplicationContext(), "MusicService created",
				Toast.LENGTH_LONG).show();
	}

	@Override
	public void onDestroy() {
		Toast.makeText(getApplicationContext(), "MusicService stopped",
				Toast.LENGTH_LONG).show();
	}

}
