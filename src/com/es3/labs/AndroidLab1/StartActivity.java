package com.es3.labs.AndroidLab1;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.EmptyStackException;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Stack;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.graphics.Color;
import android.media.*;
import android.media.MediaPlayer.OnCompletionListener;
import android.widget.*;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.AdapterView.OnItemClickListener;

import org.apache.commons.io.FileUtils;

enum PLAYER_ACTION {
	PLAYER_ACTION_NEXT,
	PLAYER_ACTION_PREV,
	PLAYER_ACTION_STOP,
	PLAYER_ACTION_TOGGLE
};

public class StartActivity extends Activity implements OnClickListener {
	
	MediaPlayer player;
	List<File> files;
	List<String> fileNames;
	
	ListView trackList;
	
	boolean isPlaying, isPaused, shuffle;
	int index = 0; // currently played track index

	Stack<Integer> navHistory = new Stack<Integer>();
	
    private Handler mHandler = new Handler();
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        int[] buttons = {R.id.prevButton, R.id.toggleButton, R.id.stopButton,
        		R.id.nextButton, R.id.shuffleCb};
        for (int btnId : buttons) {
        	findViewById(btnId).setOnClickListener(this);
        }
        
        player = new MediaPlayer();
        files = new LinkedList<File>();
        fileNames = new LinkedList<String>();
        
        listAvailableMP3s();
        for (File f : files) { fileNames.add(f.getName()); }
        
        setSong();
        player.start();
        isPlaying = true;
        updateToggleBtn();
        
        trackList = (ListView)findViewById(R.id.TrackView);
        trackList.setAdapter(new ArrayAdapter<String>(this, R.layout.track,
        		fileNames));
        trackList.setOnItemClickListener(new OnItemClickListener() {
        	public void onItemClick(AdapterView<?>l, View v, int pos, long id) {
        		index = pos;
        		jump();
        	}
        });
        
        mHandler.postDelayed(new Runnable() {
        	public void run() {
                updateTrackView();
        	}
        }, 100);
        mHandler.post(mUpdateTimeTask);
        
		player.setOnCompletionListener(new OnCompletionListener() {
			public void onCompletion(MediaPlayer mp) {
				controlPlayer(PLAYER_ACTION.PLAYER_ACTION_NEXT);
			}
		});
		
		Intent serviceIntent = new Intent(this, MusicService.class);
		startService(serviceIntent);
    }

    private void updateTrackView() {
    	trackList.setSelection(index);
    	for (int i = 0; i < trackList.getChildCount(); i++) {
    		TextView tv = (TextView) trackList.getChildAt(i);
    		tv.setTextColor(Color.GRAY);
    	}
    	TextView sel = (TextView)trackList.getChildAt(index);
    	if (sel != null)
    		sel.setTextColor(Color.WHITE);
    	
    }
    
    private void listAvailableMP3s() {
    	File path = new File("/sdcard/media");
    	String[] ext = {"mp3"};
    	files.addAll(FileUtils.listFiles(path, ext, true));
    }
    
    public void controlPlayer(PLAYER_ACTION action) {
    	switch(action) {
    	case PLAYER_ACTION_STOP:  {
			player.reset();
			isPlaying = isPaused = false;
			break;
		}
    	case PLAYER_ACTION_PREV: {
    		index -= 1;
    		
    		if (shuffle)
    			try {
    				index = navHistory.pop();
    			} catch(EmptyStackException e) {} // don't care about new index
    		jump();
    		break;
    	}
    	case PLAYER_ACTION_TOGGLE: {
    		if (isPlaying) {
    			player.pause();
    			isPaused = true;
    		} else {
    			if (isPaused) {
    				player.start();
    				isPaused = false;
    			} else {
    				jump();
    				isPlaying = false; // ugly hack just to make it inverse after 2 lines
    			}	
    		}
    		isPlaying = !isPlaying;
    		break;
    	}
    	case PLAYER_ACTION_NEXT: {
    		if (shuffle)
    			navHistory.push(index);
    		jumpBy(shuffle ? new Random().nextInt(files.size()) : 1);
    		break;
    	}
    	
    	}
		if (isPlaying) isPaused = false;
		updateToggleBtn();
    }
    
	public void onClick(View v) {
		switch(v.getId()) {
		case R.id.stopButton:
			controlPlayer(PLAYER_ACTION.PLAYER_ACTION_STOP); break;
		case R.id.prevButton:
			controlPlayer(PLAYER_ACTION.PLAYER_ACTION_PREV); break;
		case R.id.toggleButton:
			controlPlayer(PLAYER_ACTION.PLAYER_ACTION_TOGGLE); break;
		case R.id.nextButton:
			controlPlayer(PLAYER_ACTION.PLAYER_ACTION_NEXT); break;
		case R.id.shuffleCb: shuffle = ((CheckBox)v).isChecked(); break;
		}
	}
	
	void updateToggleBtn() {
		((Button)findViewById(R.id.toggleButton)).setText(isPlaying? "Pause" : "Play");
	}
	
	void jumpBy(int offset) {
		index += offset; // usually forward or backward by one song
		jump();
	}
	
	void jump() {
		Log.d("act", files.size() + ", index: " + index);
		index += (index < 0) ? files.size() : 0; // since modulus in Java can be negative
		index %= files.size(); // looping around
		setSong();
		isPlaying = true;
		player.start();
		updateTrackView();
		Log.d("act", files.size() + ", index: " + index);
	}
	
	private void setSong() {
        try {
        	if (isPlaying) player.reset();
        	isPlaying = false;
			player.setDataSource(new FileInputStream(files.get(index)).getFD());
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

        try {
			player.prepare();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
    private Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
        	if (isPlaying || isPaused) {
            	ProgressBar pb = (ProgressBar)findViewById(R.id.progressBar1);
        		pb.setProgress(player.getCurrentPosition());
        		pb.setMax(player.getDuration());
        	}
    		mHandler.postDelayed(this, 200);
        }
    };
    
    @Override
    public void onDestroy() {
    	Intent serviceIntent = new Intent(this, MusicService.class);
    	stopService(serviceIntent);
    }
}